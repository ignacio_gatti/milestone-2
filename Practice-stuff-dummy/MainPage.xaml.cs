﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Practice_stuff
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        
        public MainPage()
        {
            this.InitializeComponent();
            var subject = new List<string>();
            subject.Add("");
            subject.Add("Internet Connection");
            subject.Add("Troubleshooting");
            subject.Add("Extensions and modifications");
            subject.Add("Network Problems");
            subject.Add("Other");
            cbxSubject.ItemsSource = subject;

            cbxSubject.SelectedItem = subject[0];

        }

        //*******************SUMBIT BUTTON LOGIC*********************************
        private async void btnSumbit_Click(object sender, RoutedEventArgs e)
        {
            var errorMessage = new MessageDialog("");

            if (txtToField.Text == "" || txtFromField.Text == "" || cbxSubject.SelectedItem.ToString() == "" || txtMessage.Text == "")
            {
                errorMessage = new MessageDialog("Sorry! You Have To Fill All The Required Fields");

                errorMessage.Commands.Add(new UICommand("Continue"));
                await errorMessage.ShowAsync();
            }


            else
            {
                var messageConcatenate = "To:  " + txtToField.Text + "\n\n" + "From:  " + txtFromField.Text + "\n\n" + "About:  " + cbxSubject.SelectedItem.ToString() + "\n\n" + "Your Message:  " + txtMessage.Text;

                var messagedialog = new MessageDialog(messageConcatenate);


                messagedialog.Commands.Add(new UICommand("Send To " + txtToField.Text, SendToProcess ));
                messagedialog.Commands.Add(new UICommand("Cancel"));
                messagedialog.Commands.Add(new UICommand("RESET", ResetProcess));

                await messagedialog.ShowAsync();
            }


        }
        //*******************END OF SUMBIT BUTTON LOGIC*********************************


        //********************SEND TO BUTTON LOGIC*************************************
        private async void SendToProcess(IUICommand userInput)
        {
            string SendToMessage = "Thank You! We Will Respond As Soon As Possible";
            var messagedialog = new MessageDialog(SendToMessage);
            
            await messagedialog.ShowAsync();
            Application.Current.Exit();

        }
        //********************END OF SEND TO BUTTON LOGIC*************************************


        //********************RESET BUTTON LOGIC*************************************
        void ResetProcess(IUICommand userInput)
        {
            //reset message
            txtToField.Text = "";
            txtFromField.Text = "";
            cbxSubject.SelectedItem = "";
            txtMessage.Text = "";
        }

        //********************END OF SEND TO BUTTON LOGIC*************************************
    }
}

